using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGrue : MonoBehaviour
{

    public float torque = 250f;
    public float forceChariot = 500f;
    public float forceMoufle = 500f;
    public ArticulationBody pivot;
    public ArticulationBody chariot;
    public ArticulationBody moufle;
    public Camera[] cameras;  // Un tableau pour contenir vos 4 cam�ras
    private int currentCameraIndex = 0;

    void Start()
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].enabled = (i == 0);
        }
    }


    void Update()
    {
        //commande pour la fleche
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            pivot.AddTorque(transform.up * -torque);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            pivot.AddTorque(transform.up * torque);
        }
        //commande pour le chariot
        if (Input.GetKey(KeyCode.UpArrow))
        {
            chariot.AddRelativeForce(transform.right * forceChariot);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            chariot.AddRelativeForce(transform.right * -forceChariot);
        }
        //commande pour la moufle
        if (Input.GetKey(KeyCode.LeftShift))
        {
            moufle.AddRelativeForce(transform.up * forceMoufle);
        }
        if (Input.GetKey(KeyCode.LeftControl))
        {
            moufle.AddRelativeForce(transform.up * -forceMoufle);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex = (currentCameraIndex + 1) % cameras.Length;
            cameras[currentCameraIndex].enabled = true;
        }

    }

}
